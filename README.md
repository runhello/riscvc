# riscvc

Minimal RISCV RV32I emulator. For my own learning purposes.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/riscvc).**
